/**
 * Created by aleksejpluhin on 30.10.16.
 */
function myRender(settings){
    var $target = $(settings.target);
    var $template = $(settings.template);
    var templateString = $template.html();
    var renderHelper = _.template(templateString);
    var renderedContent = renderHelper(settings.json);
    $target.html(renderedContent);
};


function myAsyncRender(settings){
    console.log(settings);
    var isPrefetchExists = typeof settings.prefetch == 'function';
    $.ajax({
        url: settings.url,
        method: "GET" ,
        success: function(data){
            if (isPrefetchExists){
                newData = settings.prefetch(data)
            }else {
                newData = data
            }
            myRender({
                target : settings.target,
                template : settings.template,
                json : newData
            })
        },
        fail:function(data){
            console.error(data);
        }
    });
}

$(document).ready(function() {
    myAsyncRender({
        url: "/transaction",
        target: "#target-right",
        template: "#t-transactions",
    });



});


$(document).on("click", "#target-top .list-group-item", function() {
    var text = $(this).text();
    $("#target-top .list-group-item").removeClass('list-group-item-success');
    $(this).addClass('list-group-item-success');
    var id = $(this).find('label').text();
    getFilmsById(id);
});
$(document).on("click", "#target-films .pagination a", function() {

    var page = $(this).text();
    var id = $("#target-top .list-group-item-success").find("label").text();

    getFilmsById(id, page);


});
