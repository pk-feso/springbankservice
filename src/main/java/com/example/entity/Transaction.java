package com.example.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "Transaction")
@Getter
@ToString
@NoArgsConstructor
public class Transaction {
    @Id
    @GeneratedValue
    private Long id;
    private  String describe;
    private  Long price;
    private  LocalDateTime date;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id", nullable = false)
    private  User user;

    public Transaction(String describe, Long price, LocalDateTime date, User user) {
        this.describe = describe;
        this.price = price;
        this.date = date;
        this.user = user;
    }


}
