package com.example.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "User")
@Getter
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue
    @Column(name = "user_id")
    private Long id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private LocalDate birthday;
    private long availableMoney;
    @JoinColumn(name = "user_id")
    @OneToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private List<Transaction> transactions = new ArrayList<>();

    public User(String login, String password, String firstName, String lastName, LocalDate birthday) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
    }

    public void deposit(Long money) {
        availableMoney += money;
    }

    public void withdraw(Long money) {
        availableMoney -= money;
    }


}
