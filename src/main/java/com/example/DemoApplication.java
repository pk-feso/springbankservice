package com.example;

import com.example.entity.Transaction;
import com.example.entity.User;
import com.example.repository.UserRepository;
import com.example.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;

@SpringBootApplication
public class DemoApplication {



	@Autowired
	UserRepository userRepository;
	@Autowired
	TransactionService transactionService;

	@PostConstruct
	public void testRepo() {

		User user = new User("admin", "test", "al", "pl", LocalDate.now());
		userRepository.save(user);
		Transaction transaction1 = new Transaction("Buy1", 100l, LocalDateTime.now(), user);
		Transaction transaction2 = new Transaction("Buy2", 150l, LocalDateTime.now(), user);
		Transaction transaction3 = new Transaction("Buy3", 130l, LocalDateTime.now(), user);

//	transactionService.withdrawTransaction(transaction1);
//		transactionService.withdrawTransaction(transaction2);
//		transactionService.withdrawTransaction(transaction3);
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
