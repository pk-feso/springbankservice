package com.example.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

@AllArgsConstructor
@Getter
public class UserDto {

    private String firstName;
    private String lastName;
    private LocalDate birthday;
    private long availableMoney;

}
