package com.example.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;


@AllArgsConstructor
@Getter
public class TransactionDto {
    private final String describe;
    private final long price;
    private final LocalDateTime dateTime;
    private final long userId;


}
