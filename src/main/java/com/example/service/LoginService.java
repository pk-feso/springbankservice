package com.example.service;

import com.example.entity.User;


public interface LoginService {
    User checkLogin(String login, String password);
}
