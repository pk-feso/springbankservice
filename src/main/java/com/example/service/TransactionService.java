package com.example.service;

import com.example.dto.TransactionDto;
import com.example.entity.Transaction;
import com.example.entity.User;

import java.util.List;


public interface TransactionService {

    List<TransactionDto> getTransactionByUser(User user);

    void withdrawTransaction(Transaction transaction);

    void depositTransaction(Transaction transaction);
}
