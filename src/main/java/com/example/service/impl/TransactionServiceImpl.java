package com.example.service.impl;

import com.example.dto.TransactionDto;
import com.example.entity.Transaction;
import com.example.entity.User;
import com.example.repository.TransactionRepository;
import com.example.repository.UserRepository;
import com.example.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    TransactionRepository repository;
    @Autowired
    UserRepository userRepository;


    public List<TransactionDto> getTransactionByUser(User user) {
        return repository.findByUser(user).stream()
                .map(s -> new TransactionDto(s.getDescribe(), s.getPrice(), s.getDate(), s.getUser().getId()))
                .collect(Collectors.toList());
    }

    @Transactional
    public void withdrawTransaction(Transaction transaction) {
        User user = transaction.getUser();
        if(user.getAvailableMoney() <= transaction.getPrice()) {
            throw new RuntimeException("Not enough money");
        }
        user.withdraw(transaction.getPrice());
        saveConsistency(transaction, user);
    }

    @Override
    public void depositTransaction(Transaction transaction) {
        User user = transaction.getUser();
        user.deposit(transaction.getPrice());
        saveConsistency(transaction, user);
    }

    private void saveConsistency(Transaction transaction, User user) {
        userRepository.save(user);
        repository.save(transaction);
    }

}
