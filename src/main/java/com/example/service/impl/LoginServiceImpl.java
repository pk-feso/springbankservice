package com.example.service.impl;

import com.example.entity.User;
import com.example.repository.UserRepository;
import com.example.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    UserRepository repository;

    public User checkLogin(String login, String password) {
        User user = repository.findByLoginAndPassword(login, password);
        if (user == null) {
            throw new RuntimeException("User not found");
        }
        return user;
    }

}
