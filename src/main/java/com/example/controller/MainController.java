package com.example.controller;


import com.example.dto.TransactionDto;
import com.example.dto.UserDto;
import com.example.entity.Transaction;
import com.example.entity.User;
import com.example.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class MainController {

    @Autowired
    TransactionService service;


    @RequestMapping(value = "/", method = GET)
    public String register() {
        return "index";
    }

    @RequestMapping(value = "/transaction", method = POST)
    @ResponseBody
    public String saveTransaction(@RequestParam(name = "describe") String describe,
                                  @RequestParam(name = "count") Long count,
                                  HttpSession httpSession) {
        User user = (User) httpSession.getAttribute("user");
        service.withdrawTransaction(new Transaction(describe, count, LocalDateTime.now(), user));
        return "Ok";
    }


    @RequestMapping(value = "/transaction", method = GET)
    @ResponseBody
    public List<TransactionDto> getAllTransactionByUser(HttpSession session) {
        User user = (User) session.getAttribute("user");
        return service.getTransactionByUser(user);
    }

    @RequestMapping(value = "/deposit", method = POST)
    @ResponseBody
    public boolean deposit(@RequestParam(name = "count") Long count,
                           HttpSession httpSession) {
        User user = (User) httpSession.getAttribute("user");
        service.depositTransaction(new Transaction("deposit", count, LocalDateTime.now(), user));
        return true;
    }

    @RequestMapping(value = "/info", method = GET)
    @ResponseBody
    public UserDto getInfoUser(HttpSession session) {
        User user = (User) session.getAttribute("user");
        return new UserDto(user.getFirstName(), user.getLastName(), user.getBirthday(), user.getAvailableMoney());
    }

}

