package com.example.controller;

import com.example.entity.User;
import com.example.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class RegisterController {

    @Autowired
    UserRepository userRepository;


    @RequestMapping(value = "/register", method = POST, headers = "Content-Type=application/x-www-form-urlencoded")
    public String regist(@RequestParam(name = "name") String name,
                         @RequestParam(name = "password") String password,
                         @RequestParam(name = "first_name") String firstName,
                         @RequestParam(name = "last_name") String lastName,
                         @RequestParam(name = "birth") @DateTimeFormat(pattern = "dd.MM.yyyy") LocalDate birth) {

        User entity = userRepository.findByLogin(name);
        if (entity == null) {
            userRepository.save(new User(name, password, firstName, lastName, birth));
            return "index";
        } else {
            return "register";
        }
    }

    @RequestMapping(value = "/register", method = GET)
    public String registerPage() {
        return "register";
    }
}
