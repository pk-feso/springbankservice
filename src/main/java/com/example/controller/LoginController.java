package com.example.controller;

import com.example.entity.User;
import com.example.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class LoginController {

    @Autowired
    LoginService loginService;

    @RequestMapping(value = "/login", method = POST)
    public String login(@RequestParam(name = "login") String login,
                        @RequestParam(name = "password") String password,
                        HttpSession httpSession) {
        User auth = loginService.checkLogin(login, password);
        httpSession.setAttribute("user", auth);
        return "index";
    }


    @RequestMapping(value = "/login", method = GET)
    public String login() {
        return "login";
    }

}
